package id.jbs.kikoeru;

/**
 * Created by shivam on 29/5/17.
 */
public interface Constants {

    //Prefs
    String PREF_FILE_NAME = "kikoeru_pref_file";
    String PREF_ACCESS_TOKEN = "pref_access_token";
    String PREF_IS_LOGGED_IN = "pref_is_logged_in";
    String PREF_USERNAME = "pref_username";
    String PREF_EMAIL = "pref_email";
    String PREF_USER_ID = "pref_user_id";
    String PREF_USER_PHONE = "pref_user_phone";

    //URL
    String URL_HOSPITAL_ARCGIS = "http://api.mainapi.net/arcgis/0.0.2/35/query/1000?geometry=115.16322165727615,-8.798795347850605";
    String URL_MEDICAL_ARCGIS = "http://api.mainapi.net/arcgis/0.0.2/41/query/1000?geometry=115.16322165727615,-8.798795347850605";
    String URL_PHARMACY_ARCGIS = "http://api.mainapi.net/arcgis/0.0.2/50/query/1000?geometry=115.16322165727615,-8.798795347850605";

    String URL_SMSOTP_PUT = "https://api.mainapi.net/smsotp/1.0.1/otp/kikoeru";
    String URL_SMSOTP_VERIFICATION = "https://api.mainapi.net/smsotp/1.0.1/otp/kikoeru/verifications";

    String URL_SMS_NOTIF = "https://api.mainapi.net/smsnotification/1.0.0/messages";

    //api response
    String RESPONSE_SUCCESSS = "success";
    String RESPONSE_ERROR = "error";
}
