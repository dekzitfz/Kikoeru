package id.jbs.kikoeru;

import android.app.Application;
import android.content.Context;

import com.squareup.leakcanary.LeakCanary;

import id.jbs.kikoeru.injection.component.AppComponent;
import id.jbs.kikoeru.injection.component.DaggerAppComponent;
import id.jbs.kikoeru.injection.module.AppModule;
import id.jbs.kikoeru.injection.module.NetworkModule;
import timber.log.Timber;

public class MvpStarterApplication extends Application {

    private AppComponent appComponent;

    public static MvpStarterApplication get(Context context) {
        return (MvpStarterApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public AppComponent getComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .networkModule(new NetworkModule(this, BuildConfig.BASE_URL))
                    .appModule(new AppModule(this))
                    .build();
        }
        return appComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(AppComponent appComponent) {
        this.appComponent = appComponent;
    }
}
