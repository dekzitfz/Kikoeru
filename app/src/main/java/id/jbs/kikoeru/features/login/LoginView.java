package id.jbs.kikoeru.features.login;

import id.jbs.kikoeru.features.base.MvpView;

/**
 * Created by DEKZ on 1/20/2018.
 */

public interface LoginView extends MvpView {

    void onLoginSuccess();

    void onLoginFailed(String message);

}
