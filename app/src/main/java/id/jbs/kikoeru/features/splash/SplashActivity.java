package id.jbs.kikoeru.features.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import javax.inject.Inject;

import id.jbs.kikoeru.R;
import id.jbs.kikoeru.features.base.BaseActivity;
import id.jbs.kikoeru.features.home.HomeActivity;
import id.jbs.kikoeru.features.login.LoginActivity;
import id.jbs.kikoeru.injection.component.ActivityComponent;

/**
 * Created by DEKZ on 1/20/2018.
 */

public class SplashActivity extends BaseActivity implements SplashView {

    @Inject SplashPresenter mPresenter;

    @Override
    protected int getLayout() {
        return R.layout.activity_splash;
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
        mPresenter.getToken();
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onRedirectToHome() {
        new Handler().postDelayed(() -> {
            startActivity(new Intent(SplashActivity.this, HomeActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        }, 1500);
    }

    @Override
    public void onGetTokenFail(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        onRedirectToHome();
    }
}
