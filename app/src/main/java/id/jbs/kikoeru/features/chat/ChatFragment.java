package id.jbs.kikoeru.features.chat;

import android.view.View;
import android.widget.Toast;

import com.github.bassaer.chatmessageview.model.Message;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import id.jbs.kikoeru.R;
import id.jbs.kikoeru.data.model.dummy.chat.ChatItem;
import id.jbs.kikoeru.data.model.dummy.chat.User;
import id.jbs.kikoeru.features.base.BaseFragment;
import id.jbs.kikoeru.injection.component.FragmentComponent;

/**
 * Created by DEKZ on 2/9/2018.
 */

public class ChatFragment extends BaseFragment implements ChatView {

    @Inject ChatPresenter mPresenter;

    @BindView(R.id.chat_view) com.github.bassaer.chatmessageview.view.ChatView chatView;

    public ChatFragment() {
    }

    @Override
    public void onStart() {
        super.onStart();

        int myId = 0;
        String myName = "Me";

        int yourId = 1;
        String yourName = "atsega";

        final User me = new User(myId, myName);
        final User you = new User(yourId, yourName);

        Message message1 = new Message.Builder()
                .hideIcon(true)
                .setUser(me) // Sender
                .setRightMessage(true) // This message Will be shown right side.
                .setMessageText("I need some help") //Message contents
                .build();
        Message message2 = new Message.Builder()
                .hideIcon(true)
                .setUser(you) // Sender
                .setRightMessage(false) // This message Will be shown left side.
                .setMessageText("sure, How can I help?") //Message contents
                .build();

        chatView.send(message1); // Will be shown right side
        chatView.receive(message2); // Will be shown left side

        chatView.setOnClickSendButtonListener(
                v -> Toast.makeText(getActivity(), "Feature in progress!", Toast.LENGTH_SHORT).show()
        );
    }

    @Override
    public void onChatLoaded(List<ChatItem> data) {

    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_chat;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }
}
