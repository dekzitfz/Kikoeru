package id.jbs.kikoeru.features.timeline.viewholder;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.jbs.kikoeru.R;
import id.jbs.kikoeru.data.model.dummy.timeline.Post;

/**
 * Created by DEKZ on 2/9/2018.
 */

public class PostImageViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.post_username)TextView username;
    @BindView(R.id.post_image)ImageView image;
    @BindView(R.id.post_caption)TextView caption;

    public PostImageViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(Post post){
        username.setText(post.username);
        caption.setText(post.captionPost);

        String imageURI = "@drawable/" + post.imagePost;
        final Context c = itemView.getContext();
        int imgRes = c.getResources().getIdentifier(imageURI, null, c.getPackageName());
        Drawable img = c.getResources().getDrawable(imgRes);
        image.setImageDrawable(img);
    }

}
