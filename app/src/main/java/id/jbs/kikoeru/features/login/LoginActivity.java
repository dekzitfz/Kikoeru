package id.jbs.kikoeru.features.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import id.jbs.kikoeru.R;
import id.jbs.kikoeru.features.base.BaseActivity;
import id.jbs.kikoeru.features.home.HomeActivity;
import id.jbs.kikoeru.features.register.RegisterActivity;
import id.jbs.kikoeru.features.verifyotp.VerifyOtpActivity;
import id.jbs.kikoeru.injection.component.ActivityComponent;

/**
 * Created by DEKZ on 1/20/2018.
 */

public class LoginActivity
        extends BaseActivity
        implements LoginView, Validator.ValidationListener {

    @Inject LoginPresenter mPresenter;

    @BindView(R.id.toolbar)Toolbar toolbar;
    @Email @BindView(R.id.et_email)EditText email;
    @Password @BindView(R.id.et_password)EditText password;

    private Validator mValidator;
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) getSupportActionBar().setTitle("Login");

        mValidator = new Validator(this);
        mValidator.setValidationListener(this);
    }

    @Override
    public void onLoginSuccess() {
        if(mProgress.isShowing()){
            mProgress.dismiss();
        }
        startActivity(new Intent(this, VerifyOtpActivity.class));
    }

    @Override
    public void onLoginFailed(String message) {
        if(mProgress.isShowing()){
            mProgress.dismiss();
        }
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_login;
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @OnClick(R.id.tv_goto_register)
    public void goToRegister(){
        startActivity(new Intent(this, RegisterActivity.class));
    }

    @OnClick(R.id.btn_login)
    public void loginClick(){
        if(mValidator!=null) mValidator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        if(mProgress == null){
            mProgress = new ProgressDialog(this);
        }
        mProgress.setMessage("Please Wait");
        mProgress.setIndeterminate(true);
        mProgress.show();

        mPresenter.login(
                email.getText().toString(),
                password.getText().toString()
        );
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
