package id.jbs.kikoeru.features.register;

import javax.inject.Inject;

import id.jbs.kikoeru.Constants;
import id.jbs.kikoeru.data.DataManager;
import id.jbs.kikoeru.features.base.BasePresenter;
import id.jbs.kikoeru.injection.ConfigPersistent;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by DEKZ on 1/20/2018.
 */

@ConfigPersistent
public class RegisterPresenter extends BasePresenter<RegisterView> {

    private final DataManager dataManager;

    @Inject
    public RegisterPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(RegisterView mvpView) {
        super.attachView(mvpView);
    }

    void register(String email, String username, String phone, String password){
        dataManager.registerUser(username, email, phone, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(this::addDisposable)
                .subscribe(
                        response -> {
                            if(response.getStatus().equals(Constants.RESPONSE_SUCCESSS)){
                                getView().onRegisterSuccess();
                            }else{
                                getView().onRegisterFailed(response.getMessage());
                            }
                        },
                        error -> {
                            Timber.w(error.getMessage());
                            getView().onRegisterFailed("Something Wrong Happened");
                        }
                );
    }
}
