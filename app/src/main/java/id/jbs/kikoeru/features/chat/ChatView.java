package id.jbs.kikoeru.features.chat;

import java.util.List;

import id.jbs.kikoeru.data.model.dummy.chat.ChatItem;
import id.jbs.kikoeru.features.base.MvpView;

/**
 * Created by DEKZ on 2/9/2018.
 */

public interface ChatView extends MvpView {

    void onChatLoaded(List<ChatItem> data);

}
