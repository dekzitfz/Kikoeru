package id.jbs.kikoeru.features.timeline;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import id.jbs.kikoeru.R;
import id.jbs.kikoeru.data.model.dummy.timeline.Post;
import id.jbs.kikoeru.features.base.BaseFragment;
import id.jbs.kikoeru.injection.component.FragmentComponent;

/**
 * Created by DEKZ on 2/8/2018.
 */

public class TimelineFragment extends BaseFragment implements TimelineView {

    @Inject TimelinePresenter mPresenter;

    private TimelineAdapter adapter;

    @BindView(R.id.rv_timeline)RecyclerView rvTimeline;

    public TimelineFragment() {}

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.getData();
    }

    @Override
    public void onTimeLineLoaded(List<Post> data) {
        adapter = new TimelineAdapter();
        adapter.setData(data);
        rvTimeline.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTimeline.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onTimeLineLoadFailed(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_timeline;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }
}
