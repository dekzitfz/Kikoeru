package id.jbs.kikoeru.features.timeline;

import java.util.List;

import id.jbs.kikoeru.data.model.dummy.timeline.Post;
import id.jbs.kikoeru.features.base.MvpView;

/**
 * Created by DEKZ on 2/8/2018.
 */

public interface TimelineView extends MvpView {

    void onTimeLineLoaded(List<Post> data);

    void onTimeLineLoadFailed(String message);

}
