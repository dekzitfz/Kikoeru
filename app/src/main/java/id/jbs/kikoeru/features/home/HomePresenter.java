package id.jbs.kikoeru.features.home;

import javax.inject.Inject;

import id.jbs.kikoeru.data.DataManager;
import id.jbs.kikoeru.features.base.BasePresenter;
import id.jbs.kikoeru.injection.ConfigPersistent;

/**
 * Created by DEKZ on 2/8/2018.
 */

@ConfigPersistent
public class HomePresenter extends BasePresenter<HomeView> {

    private final DataManager dataManager;

    @Inject
    public HomePresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    void checkLoginSession(){
        if(!dataManager.getLoginSession()){
            getView().onNoLoginSession();
        }else{
            getView().onUserHasLoginSession(dataManager.getLoggedInUsername());
        }
    }

    void logout(){
        dataManager.clearLoginSession();
        getView().onUserLogout();
    }
}
