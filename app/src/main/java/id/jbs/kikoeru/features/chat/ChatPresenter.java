package id.jbs.kikoeru.features.chat;

import javax.inject.Inject;

import id.jbs.kikoeru.data.DataManager;
import id.jbs.kikoeru.features.base.BasePresenter;
import id.jbs.kikoeru.injection.ConfigPersistent;

/**
 * Created by DEKZ on 2/9/2018.
 */

@ConfigPersistent
public class ChatPresenter extends BasePresenter<ChatView> {

    private final DataManager dataManager;

    @Inject
    public ChatPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }
}
