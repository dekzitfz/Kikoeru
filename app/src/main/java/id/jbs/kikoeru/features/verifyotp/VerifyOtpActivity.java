package id.jbs.kikoeru.features.verifyotp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import id.jbs.kikoeru.R;
import id.jbs.kikoeru.features.base.BaseActivity;
import id.jbs.kikoeru.features.home.HomeActivity;
import id.jbs.kikoeru.injection.component.ActivityComponent;

/**
 * Created by DEKZ on 2/16/2018.
 */

public class VerifyOtpActivity extends BaseActivity implements OtpView {

    @Inject OtpPresenter mPresenter;

    @BindView(R.id.toolbar)Toolbar toolbar;
    @BindView(R.id.et_otp_code)EditText OTPCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Verify SMS OTP");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public void onVerified() {
        startActivity(new Intent(this, HomeActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    @Override
    public void onFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_opt;
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @OnClick(R.id.btn_verify_otp)
    public void verifyClick(){
        if(OTPCode.getText().length() < 4){
            Toast.makeText(this, "please fill all 4 digit code", Toast.LENGTH_SHORT).show();
        }else{
            mPresenter.verifyCodeOtp(OTPCode.getText().toString());
        }
    }
}
