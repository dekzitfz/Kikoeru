package id.jbs.kikoeru.features.verifyotp;

import javax.inject.Inject;

import id.jbs.kikoeru.data.DataManager;
import id.jbs.kikoeru.features.base.BasePresenter;
import id.jbs.kikoeru.injection.ConfigPersistent;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by DEKZ on 2/16/2018.
 */

@ConfigPersistent
public class OtpPresenter extends BasePresenter<OtpView> {

    private final DataManager dataManager;

    @Inject
    public OtpPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    void verifyCodeOtp(String code){
        dataManager.verifySMSOTP(code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(this::addDisposable)
                .subscribe(
                        response -> {
                            if(response.isSuccessful()){
                                dataManager.createLoginSession();
                                getView().onVerified();
                            }else{
                                getView().onFailed("Something Wrong Happened, please try again");
                            }
                        },
                        err -> {
                            Timber.w(err.getMessage());
                            getView().onFailed("Something Wrong Happened, please try again");
                        }
                );
    }
}
