package id.jbs.kikoeru.features.home;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.jbs.kikoeru.R;
import id.jbs.kikoeru.features.base.BaseActivity;
import id.jbs.kikoeru.features.chat.ChatFragment;
import id.jbs.kikoeru.features.login.LoginActivity;
import id.jbs.kikoeru.features.timeline.TimelineFragment;
import id.jbs.kikoeru.injection.component.ActivityComponent;
import timber.log.Timber;

public class HomeActivity extends BaseActivity
        implements HomeView, NavigationView.OnNavigationItemSelectedListener {

    @Inject HomePresenter mPresenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.fab_create) FloatingActionButton fabCreate;
    private ImageView userImage;
    private TextView userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportActionBar().setTitle("Timeline");

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_home;
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
        mPresenter.checkLoginSession();
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_timeline) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_container, new TimelineFragment())
                    .commit();
            fabCreate.setVisibility(View.VISIBLE);
            getSupportActionBar().setTitle("Timeline");
        } else if (id == R.id.nav_chat) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_container, new ChatFragment())
                    .commit();
            fabCreate.setVisibility(View.GONE);
            getSupportActionBar().setTitle("Chat");
        } else if (id == R.id.nav_logout){
            mPresenter.logout();
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @OnClick(R.id.fab_create)
    public void createClick(){
        Toast.makeText(this, "feature in progress", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNoLoginSession() {
        startActivity(new Intent(this, LoginActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    @Override
    public void onUserHasLoginSession(String username) {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_container, new TimelineFragment())
                .commit();

        View navHeader = navigationView.getHeaderView(0);
        userImage = navHeader.findViewById(R.id.user_image);
        userName = navHeader.findViewById(R.id.username);

        TextDrawable txtDrawable = TextDrawable.builder()
                .beginConfig()
                    .textColor(Color.WHITE)
                    .useFont(Typeface.DEFAULT)
                    .bold()
                    .toUpperCase()
                .endConfig()
                .buildRound(String.valueOf(username.charAt(0)), ColorGenerator.MATERIAL.getRandomColor());
        userImage.setImageDrawable(txtDrawable);
        userName.setText(username);
    }

    @Override
    public void onUserLogout() {
        startActivity(new Intent(this, LoginActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }
}
