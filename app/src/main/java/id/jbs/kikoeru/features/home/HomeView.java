package id.jbs.kikoeru.features.home;

import id.jbs.kikoeru.features.base.MvpView;

/**
 * Created by DEKZ on 2/8/2018.
 */

public interface HomeView extends MvpView {

    void onNoLoginSession();

    void onUserHasLoginSession(String username);

    void onUserLogout();
}
