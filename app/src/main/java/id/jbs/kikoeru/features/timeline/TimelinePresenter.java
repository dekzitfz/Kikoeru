package id.jbs.kikoeru.features.timeline;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import id.jbs.kikoeru.data.DataManager;
import id.jbs.kikoeru.data.model.dummy.timeline.Post;
import id.jbs.kikoeru.features.base.BasePresenter;
import id.jbs.kikoeru.injection.ConfigPersistent;

/**
 * Created by DEKZ on 2/8/2018.
 */

@ConfigPersistent
public class TimelinePresenter extends BasePresenter<TimelineView> {

    private final DataManager dataManager;

    @Inject
    public TimelinePresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    void getData(){
        getView().onTimeLineLoaded(getDummyData());
    }

    private List<Post> getDummyData(){
        List<Post> dummy = new ArrayList<>();

        Post post1 = new Post();
        post1.postId = 1;
        post1.username = "xray";
        post1.captionPost = "God bless those who can sleep nicely and peacefully around this time while on the other side, there's always someone who alrd usrek kanan kiri kayang salto just in attempts to be able to sleep like JUST FOR AN HOUR GDI )O)";
        post1.timestamp = "";
        post1.imagePost = "";

        Post post2 = new Post();
        post2.postId = 2;
        post2.username = "zayk_";
        post2.captionPost = "Baiknya kita harus bijak dalam menggunakan sosial media dan mari kita coba untuk mengurangi penggunaan untuk kesehatan mental yang lebih baik.";
        post2.timestamp = "";
        post2.imagePost = "sample_img_timeline";

        dummy.add(post1);
        dummy.add(post2);

        return dummy;
    }
}
