package id.jbs.kikoeru.features.splash;

import id.jbs.kikoeru.features.base.MvpView;

/**
 * Created by DEKZ on 2/10/2018.
 */

public interface SplashView extends MvpView {

    void onRedirectToHome();

    void onGetTokenFail(String message);
}
