package id.jbs.kikoeru.features.register;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import id.jbs.kikoeru.R;
import id.jbs.kikoeru.features.base.BaseActivity;
import id.jbs.kikoeru.features.login.LoginActivity;
import id.jbs.kikoeru.injection.component.ActivityComponent;

/**
 * Created by DEKZ on 1/20/2018.
 */

public class RegisterActivity
        extends BaseActivity
        implements RegisterView, Validator.ValidationListener {

    @Inject RegisterPresenter mPresenter;

    @BindView(R.id.toolbar)Toolbar toolbar;
    @BindView(R.id.et_username)EditText username;
    @Email @BindView(R.id.et_email)EditText email;
    @NotEmpty @BindView(R.id.et_phone)EditText phone;
    @Password @BindView(R.id.et_password)EditText password;
    @ConfirmPassword @BindView(R.id.et_password_confirm)EditText passwordConfirm;

    private Validator mValidator;
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setTitle("Sign Up");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mValidator = new Validator(this);
        mValidator.setValidationListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public void onRegisterSuccess() {
        if(mProgress.isShowing()){
            mProgress.dismiss();
        }
        Toast.makeText(getApplicationContext(), "Sign Up Completed! Please Login", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, LoginActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    @Override
    public void onRegisterFailed(String message) {
        if(mProgress.isShowing()){
            mProgress.dismiss();
        }
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_register;
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @Override
    public void onValidationSucceeded() {
        if(mProgress == null){
            mProgress = new ProgressDialog(this);
        }
        mProgress.setMessage("Please Wait");
        mProgress.setIndeterminate(true);
        mProgress.show();

        mPresenter.register(
                email.getText().toString(),
                username.getText().toString(),
                phone.getText().toString(),
                password.getText().toString()
        );
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @OnClick(R.id.btn_register)
    public void registerClick(){
        if(mValidator!=null) mValidator.validate();
    }
}
