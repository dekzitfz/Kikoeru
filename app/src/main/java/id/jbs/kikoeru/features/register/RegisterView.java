package id.jbs.kikoeru.features.register;

import id.jbs.kikoeru.features.base.MvpView;

/**
 * Created by DEKZ on 1/20/2018.
 */

public interface RegisterView extends MvpView {

    void onRegisterSuccess();

    void onRegisterFailed(String message);

}
