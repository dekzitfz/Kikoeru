package id.jbs.kikoeru.features.verifyotp;

import id.jbs.kikoeru.features.base.MvpView;

/**
 * Created by DEKZ on 2/16/2018.
 */

public interface OtpView extends MvpView {

    void onVerified();

    void onFailed(String message);

}
