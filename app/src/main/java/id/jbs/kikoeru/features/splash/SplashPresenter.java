package id.jbs.kikoeru.features.splash;

import javax.inject.Inject;

import id.jbs.kikoeru.data.DataManager;
import id.jbs.kikoeru.features.base.BasePresenter;
import id.jbs.kikoeru.injection.ConfigPersistent;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by DEKZ on 2/10/2018.
 */

@ConfigPersistent
public class SplashPresenter extends BasePresenter<SplashView> {

    private final DataManager dataManager;

    @Inject
    public SplashPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    void getToken(){
        dataManager.getToken()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(this::addDisposable)
                .subscribe(
                        response -> {
                            dataManager.saveAccessToken(response.getAccessToken());
                            getView().onRedirectToHome();
                        },
                        error -> {
                            Timber.w(error.getMessage());
                            getView().onGetTokenFail(error.getMessage());
                        }
                );
    }
}
