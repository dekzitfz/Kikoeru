package id.jbs.kikoeru.features.timeline;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import id.jbs.kikoeru.R;
import id.jbs.kikoeru.data.model.dummy.timeline.Post;
import id.jbs.kikoeru.features.timeline.viewholder.PostImageViewHolder;
import id.jbs.kikoeru.features.timeline.viewholder.PostViewHolder;

/**
 * Created by DEKZ on 2/9/2018.
 */

public class TimelineAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_POST= 0;
    private static final int VIEW_POST_WITH_IMAGE = 1;

    private List<Post> data = new ArrayList<>();

    public TimelineAdapter() {
    }

    public List<Post> getData() {
        return data;
    }

    public void setData(List<Post> data) {
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == VIEW_POST){
            return new PostViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_post, parent, false));
        }else{
            return new PostImageViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_post_image, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final int type = getItemViewType(position);
        if(type == VIEW_POST){
            ((PostViewHolder) holder).bind(data.get(position));
        }else{
            ((PostImageViewHolder) holder).bind(data.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(data.get(position).imagePost.equals("")){
            return VIEW_POST;
        }else{
            return VIEW_POST_WITH_IMAGE;
        }
    }
}
