package id.jbs.kikoeru.features.login;

import javax.inject.Inject;

import id.jbs.kikoeru.Constants;
import id.jbs.kikoeru.data.DataManager;
import id.jbs.kikoeru.features.base.BasePresenter;
import id.jbs.kikoeru.injection.ConfigPersistent;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by DEKZ on 1/20/2018.
 */

@ConfigPersistent
public class LoginPresenter extends BasePresenter<LoginView> {

    private final DataManager dataManager;

    @Inject
    public LoginPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(LoginView mvpView) {
        super.attachView(mvpView);
    }

    void login(String email, String password){
        dataManager.loginUser(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(this::addDisposable)
                .subscribe(
                        response -> {
                            if(response.getStatus().equals(Constants.RESPONSE_SUCCESSS)){
                                dataManager.saveUserCredentials(
                                        response.getEmail(),
                                        response.getUsername(),
                                        response.getIdUser()
                                );
                                dataManager.saveOnlyPhoneNumber(response.getPhone());
                                sendPutSMSOTP(response.getPhone());
                            }else{
                                getView().onLoginFailed(response.getMessage());
                            }
                        },
                        error -> {
                            Timber.w(error.getMessage());
                            getView().onLoginFailed("Something Wrong Happened");
                        }
                );
    }

    private void sendPutSMSOTP(String phoneNum){
        dataManager.putSMSOTP(phoneNum)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(this::addDisposable)
                .subscribe(
                        response -> {
                            if(response.isSuccessful()){
                                getView().onLoginSuccess();
                            }else{
                                getView().onLoginFailed("Something Wrong Happened, please try again");
                            }
                        },
                        err -> {
                            Timber.w(err.getMessage());
                            getView().onLoginFailed("Something Wrong Happened, please try again");
                        }
                );
    }

}
