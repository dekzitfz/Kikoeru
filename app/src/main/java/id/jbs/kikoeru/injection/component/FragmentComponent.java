package id.jbs.kikoeru.injection.component;

import dagger.Subcomponent;
import id.jbs.kikoeru.features.chat.ChatFragment;
import id.jbs.kikoeru.features.timeline.TimelineFragment;
import id.jbs.kikoeru.injection.PerFragment;
import id.jbs.kikoeru.injection.module.FragmentModule;

/**
 * This component inject dependencies to all Fragments across the application
 */
@PerFragment
@Subcomponent(modules = FragmentModule.class)
public interface FragmentComponent {
    void inject(TimelineFragment timelineFragment);

    void inject(ChatFragment chatFragment);
}
