package id.jbs.kikoeru.injection.component;

import dagger.Component;
import id.jbs.kikoeru.features.base.BaseActivity;
import id.jbs.kikoeru.features.base.BaseFragment;
import id.jbs.kikoeru.injection.ConfigPersistent;
import id.jbs.kikoeru.injection.module.ActivityModule;
import id.jbs.kikoeru.injection.module.FragmentModule;

/**
 * A dagger component that will live during the lifecycle of an Activity or Fragment but it won't be
 * destroy during configuration changes. Check {@link BaseActivity} and {@link BaseFragment} to see
 * how this components survives configuration changes. Use the {@link ConfigPersistent} scope to
 * annotate dependencies that need to survive configuration changes (for example Presenters).
 */
@ConfigPersistent
@Component(dependencies = AppComponent.class)
public interface ConfigPersistentComponent {

    ActivityComponent activityComponent(ActivityModule activityModule);

    FragmentComponent fragmentComponent(FragmentModule fragmentModule);
}
