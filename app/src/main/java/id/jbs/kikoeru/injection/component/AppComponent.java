package id.jbs.kikoeru.injection.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import id.jbs.kikoeru.data.DataManager;
import id.jbs.kikoeru.data.local.PreferencesHelper;
import id.jbs.kikoeru.injection.ApplicationContext;
import id.jbs.kikoeru.injection.module.AppModule;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    @ApplicationContext
    Context context();

    Application application();

    DataManager apiManager();

    PreferencesHelper PrefHelper();
}
