package id.jbs.kikoeru.injection.component;

import dagger.Subcomponent;
import id.jbs.kikoeru.features.home.HomeActivity;
import id.jbs.kikoeru.features.login.LoginActivity;
import id.jbs.kikoeru.features.register.RegisterActivity;
import id.jbs.kikoeru.features.splash.SplashActivity;
import id.jbs.kikoeru.features.verifyotp.VerifyOtpActivity;
import id.jbs.kikoeru.injection.PerActivity;
import id.jbs.kikoeru.injection.module.ActivityModule;

@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(LoginActivity loginActivity);

    void inject(RegisterActivity registerActivity);

    void inject(HomeActivity homeActivity);

    void inject(SplashActivity splashActivity);

    void inject(VerifyOtpActivity verifyOtpActivity);
}
