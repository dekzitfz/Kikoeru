package id.jbs.kikoeru.data.model.response;

public class NamedResource {
    public String name;
    public String url;
}
