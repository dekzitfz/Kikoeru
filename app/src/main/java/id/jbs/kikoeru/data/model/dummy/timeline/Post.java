package id.jbs.kikoeru.data.model.dummy.timeline;

/**
 * Created by DEKZ on 2/8/2018.
 */

public class Post {

    public int postId;
    public String username;
    public String imagePath;
    public String timestamp;
    public String captionPost;
    public String imagePost;
}
