package id.jbs.kikoeru.data.model.response.smsotp.put;

import com.google.gson.annotations.SerializedName;

public class OTPPutResponse{

	@SerializedName("maxAttempt")
	private String maxAttempt;

	@SerializedName("msgId")
	private String msgId;

	@SerializedName("expireIn")
	private int expireIn;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private boolean status;

	public void setMaxAttempt(String maxAttempt){
		this.maxAttempt = maxAttempt;
	}

	public String getMaxAttempt(){
		return maxAttempt;
	}

	public void setMsgId(String msgId){
		this.msgId = msgId;
	}

	public String getMsgId(){
		return msgId;
	}

	public void setExpireIn(int expireIn){
		this.expireIn = expireIn;
	}

	public int getExpireIn(){
		return expireIn;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"OTPPutResponse{" + 
			"maxAttempt = '" + maxAttempt + '\'' + 
			",msgId = '" + msgId + '\'' + 
			",expireIn = '" + expireIn + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}