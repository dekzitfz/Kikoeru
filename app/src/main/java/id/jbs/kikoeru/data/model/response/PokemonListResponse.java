package id.jbs.kikoeru.data.model.response;

import java.util.List;

public class PokemonListResponse {
    public List<NamedResource> results;
}
