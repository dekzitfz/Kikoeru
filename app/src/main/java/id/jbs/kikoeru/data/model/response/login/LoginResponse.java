package id.jbs.kikoeru.data.model.response.login;

import com.google.gson.annotations.SerializedName;

public class LoginResponse{

	@SerializedName("phone")
	private String phone;

	@SerializedName("id_user")
	private int idUser;

	@SerializedName("message")
	private String message;

	@SerializedName("email")
	private String email;

	@SerializedName("status")
	private String status;

	@SerializedName("username")
	private String username;

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setIdUser(int idUser){
		this.idUser = idUser;
	}

	public int getIdUser(){
		return idUser;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"LoginResponse{" + 
			"phone = '" + phone + '\'' + 
			",id_user = '" + idUser + '\'' + 
			",message = '" + message + '\'' + 
			",email = '" + email + '\'' + 
			",status = '" + status + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}