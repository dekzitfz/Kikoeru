package id.jbs.kikoeru.data.model.response.queryarcgis;

import com.google.gson.annotations.SerializedName;

public class FeaturesItem{

	@SerializedName("attributes")
	private Attributes attributes;

	@SerializedName("geometry")
	private Geometry geometry;

	public void setAttributes(Attributes attributes){
		this.attributes = attributes;
	}

	public Attributes getAttributes(){
		return attributes;
	}

	public void setGeometry(Geometry geometry){
		this.geometry = geometry;
	}

	public Geometry getGeometry(){
		return geometry;
	}

	@Override
 	public String toString(){
		return 
			"FeaturesItem{" + 
			"attributes = '" + attributes + '\'' + 
			",geometry = '" + geometry + '\'' + 
			"}";
		}
}