package id.jbs.kikoeru.data.remote;

import id.jbs.kikoeru.Constants;
import id.jbs.kikoeru.data.model.response.login.LoginResponse;
import id.jbs.kikoeru.data.model.response.queryarcgis.ArcGisResponse;
import id.jbs.kikoeru.data.model.response.register.RegisterResponse;
import id.jbs.kikoeru.data.model.response.smsotp.put.OTPPutResponse;
import id.jbs.kikoeru.data.model.response.smsotp.verify.OTPVerifyResponse;
import id.jbs.kikoeru.data.model.response.token.TokenResponse;
import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Url;

public interface KikoeruService {

    @FormUrlEncoded
    @POST
    Single<TokenResponse> getToken(
            @Url String mainApiUrl,
            @Header("Authorization") String authorization,
            @Field("grant_type") String grantType
    );

    @GET(Constants.URL_HOSPITAL_ARCGIS)
    Single<ArcGisResponse> queryHospitalFromArcGis(
            @Header("Authorization") String authorization
    );

    @GET(Constants.URL_MEDICAL_ARCGIS)
    Single<ArcGisResponse> queryMedicalFromArcGis(
            @Header("Authorization") String authorization
    );

    @GET(Constants.URL_PHARMACY_ARCGIS)
    Single<ArcGisResponse> queryPharmacyFromArcGis(
            @Header("Authorization") String authorization
    );

    @FormUrlEncoded
    @PUT
    Single<Response<OTPPutResponse>> sendSMSOTP(
            @Url String putSMSOTPURL,
            @Header("Authorization") String authorization,
            @Field("phoneNum") String phoneNum,
            @Field("digit") String digit
    );

    @FormUrlEncoded
    @POST
    Single<Response<OTPVerifyResponse>> sendVerificationSMSOTP(
            @Url String url,
            @Header("Authorization") String authorization,
            @Field("otpstr") String otpstr,
            @Field("digit") String digit
    );

    @FormUrlEncoded
    @POST(Constants.URL_SMS_NOTIF)
    Single<Void> sendSMSNotification(
            @Header("Authorization") String authorization,
            @Field("msisdn") String msisdn,
            @Field("content") String content
    );


    /* ---------------------------------------------------------------------------- */

    @FormUrlEncoded
    @POST("register")
    Single<RegisterResponse> postRegister(
            @Field("username") String username,
            @Field("email") String email,
            @Field("phone") String phone,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("login")
    Single<LoginResponse> postLogin(
            @Field("email") String email,
            @Field("password") String password
    );
}
