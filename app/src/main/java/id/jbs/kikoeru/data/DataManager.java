package id.jbs.kikoeru.data;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import id.jbs.kikoeru.BuildConfig;
import id.jbs.kikoeru.Constants;
import id.jbs.kikoeru.data.local.PreferencesHelper;
import id.jbs.kikoeru.data.model.response.Pokemon;
import id.jbs.kikoeru.data.model.response.login.LoginResponse;
import id.jbs.kikoeru.data.model.response.register.RegisterResponse;
import id.jbs.kikoeru.data.model.response.smsotp.put.OTPPutResponse;
import id.jbs.kikoeru.data.model.response.smsotp.verify.OTPVerifyResponse;
import id.jbs.kikoeru.data.model.response.token.TokenResponse;
import id.jbs.kikoeru.data.remote.KikoeruService;
import io.reactivex.Single;
import retrofit2.Response;

/**
 * Created by shivam on 29/5/17.
 */
@Singleton
public class DataManager {

    private final KikoeruService kikoeruService;
    private final PreferencesHelper preferencesHelper;

    @Inject
    public DataManager(KikoeruService kikoeruService, PreferencesHelper preferencesHelper) {
        this.kikoeruService = kikoeruService;
        this.preferencesHelper = preferencesHelper;
    }

    public Single<TokenResponse> getToken(){
        return kikoeruService.getToken(
                BuildConfig.MAINAPI_API_URL+"token",
                "Basic "+BuildConfig.TOKEN_CREDENTIAL,
                BuildConfig.GRANT_TYPE
        );
    }

    public void saveAccessToken(String accessToken){
        preferencesHelper.putString(Constants.PREF_ACCESS_TOKEN, "Bearer "+accessToken);
    }

    public String getSavedAccessToken(){
        return preferencesHelper.getString(Constants.PREF_ACCESS_TOKEN);
    }

    public void createLoginSession(){
        preferencesHelper.putBoolean(Constants.PREF_IS_LOGGED_IN, true);
    }

    public void saveUserCredentials(String email, String username, int id){
        preferencesHelper.putString(Constants.PREF_EMAIL, email);
        preferencesHelper.putString(Constants.PREF_USERNAME, username);
        preferencesHelper.putInt(Constants.PREF_USER_ID, id);
    }

    public boolean getLoginSession(){
        return preferencesHelper.getBoolean(Constants.PREF_IS_LOGGED_IN);
    }

    public void clearLoginSession(){
        preferencesHelper.clear();
    }

    public Single<RegisterResponse> registerUser(
            String username, String email, String phone, String password){
        return kikoeruService.postRegister(username, email, phone, password);
    }

    public Single<LoginResponse> loginUser(String email, String password){
        return kikoeruService.postLogin(email, password);
    }

    public String getLoggedInUsername(){
        return preferencesHelper.getString(Constants.PREF_USERNAME);
    }

    public void saveOnlyPhoneNumber(String phone){
        preferencesHelper.putString(Constants.PREF_USER_PHONE, phone);
    }

    public Single<Response<OTPPutResponse>> putSMSOTP(String phoneNum){
        return kikoeruService.sendSMSOTP(
                Constants.URL_SMSOTP_PUT,
                getSavedAccessToken(),
                phoneNum,
                "4"
        );
    }

    public Single<Response<OTPVerifyResponse>> verifySMSOTP(String code){
        return kikoeruService.sendVerificationSMSOTP(
                Constants.URL_SMSOTP_VERIFICATION,
                getSavedAccessToken(),
                code,
                "4"
        );
    }
}
