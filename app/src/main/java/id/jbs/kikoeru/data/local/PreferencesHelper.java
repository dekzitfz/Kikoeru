package id.jbs.kikoeru.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Provides;
import id.jbs.kikoeru.Constants;
import id.jbs.kikoeru.injection.ApplicationContext;

@Singleton
public class PreferencesHelper {

    private static final String PREF_FILE_NAME = Constants.PREF_FILE_NAME;

    private final SharedPreferences preferences;

    @Inject
    PreferencesHelper(@ApplicationContext Context context) {
        preferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }

    public void putString(@Nonnull String key, @Nonnull String value) {
        preferences.edit().putString(key, value).apply();
    }

    public String getString(@Nonnull String key) {
        return preferences.getString(key, "");
    }

    public void putBoolean(@Nonnull String key, @Nonnull boolean value) {
        preferences.edit().putBoolean(key, value).apply();
    }

    public boolean getBoolean(@Nonnull String key) {
        return preferences.getBoolean(key, false);
    }

    public void putInt(@Nonnull String key, @Nonnull int value) {
        preferences.edit().putInt(key, value).apply();
    }

    public int getInt(@Nonnull String key) {
        return preferences.getInt(key, -1);
    }

    public void clear() {
        preferences.edit().clear().apply();
    }
}
