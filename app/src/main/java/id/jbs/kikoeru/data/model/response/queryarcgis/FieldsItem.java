package id.jbs.kikoeru.data.model.response.queryarcgis;

import com.google.gson.annotations.SerializedName;

public class FieldsItem{

	@SerializedName("lenght")
	private int lenght;

	@SerializedName("name")
	private String name;

	@SerializedName("alias")
	private String alias;

	@SerializedName("type")
	private String type;

	public void setLenght(int lenght){
		this.lenght = lenght;
	}

	public int getLenght(){
		return lenght;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setAlias(String alias){
		this.alias = alias;
	}

	public String getAlias(){
		return alias;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	@Override
 	public String toString(){
		return 
			"FieldsItem{" + 
			"lenght = '" + lenght + '\'' + 
			",name = '" + name + '\'' + 
			",alias = '" + alias + '\'' + 
			",type = '" + type + '\'' + 
			"}";
		}
}