package id.jbs.kikoeru.data.model.dummy.chat;

/**
 * Created by DEKZ on 2/9/2018.
 */

public class ChatItem {

    public int id;
    public String name;
    public String message;
    public String timestamp;

}
