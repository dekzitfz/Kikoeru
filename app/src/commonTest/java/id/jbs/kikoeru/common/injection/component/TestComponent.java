package id.jbs.kikoeru.common.injection.component;

import javax.inject.Singleton;

import dagger.Component;
import id.jbs.kikoeru.common.injection.module.ApplicationTestModule;
import id.jbs.kikoeru.injection.component.AppComponent;

@Singleton
@Component(modules = ApplicationTestModule.class)
public interface TestComponent extends AppComponent {
}
