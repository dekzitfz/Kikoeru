# KIKOERU by Letrina Team

## Deskripsi

Aplikasi Kikoeru merupakan aplikasi yang menjadi media perantara antara pasien penyakit mental dan konselor. Pasien / user dapat mengunakan aplikasi apabila mereka merasa butuh bantuan konsultasi dengan mudah menggunakan fitur aplikasi KIKOERU

## Catatan

untuk mencoba build dan jalankan app ini, generate file `gradle.properties` dan tambahkan kode dibawah ini untuk konfigurasi credentials API xsight

``` grovy
ArcGISUrl = http://api.mainapi.net/arcgis/0.0.2/
MainApiUrl = http://api.mainapi.net/

username = USERNAME_CREDENTIAL_ANDA
password = PASSWORD_CREDENTIAL_ANDA

grantType = client_credentials
tokenCredential = TOKEN_ANDA

storepassword = PASSWORD_STORE
keyalias = ALIAS
keypassword = KEY_PASSWORD
```

generate juga file `kikoeru_release.jks` diluar folder project jika ingin merilis apk production